 SmoothScroll({
     // Время скролла 400 = 0.4 секунды
     animationTime: 800,
     // Размер шага в пикселях 
     stepSize: 75,

     // Дополнительные настройки:

     // Ускорение 
     accelerationDelta: 30,
     // Максимальное ускорение
     accelerationMax: 2,

     // Поддержка клавиатуры
     keyboardSupport: true,
     // Шаг скролла стрелками на клавиатуре в пикселях
     arrowScroll: 50,

     // Pulse (less tweakable)
     // ratio of "tail" to "acceleration"
     pulseAlgorithm: true,
     pulseScale: 4,
     pulseNormalize: 1,

     // Поддержка тачпада
     touchpadSupport: true,
 })

 const controller = new ScrollMagic.Controller()
const tl = new TimelineMax();
tl.from(".left_part-header", {y:100, opacity:0, duration: 0.5})
tl.from(".header_avatar", {y:100, opacity:0, duration: 0.8})
tl.from(".header-sub-avatar", {y:100, opacity:0, duration: 1})
tl.from(".mid_part-header", {y:100, opacity:0, duration: 0.4, stagger: 0.2})

 const scene1 = new ScrollMagic.Scene()

const controller2 = new ScrollMagic.Controller()
const tl2 = new TimelineMax();
tl2.from(".main_card", {x:150, opacity:0, duration: 0.5, stagger: 0.3})

const scene2 = new ScrollMagic.Scene(
    {
        triggerElement: ".course_block",
    }
).setTween(tl2)
.addTo(controller2)


const tl3 = new TimelineMax();
const tweenTl3 = new TweenMax.from(".portfolio__item", 0.5, {scale:0, opacity: 0, stagger: 0.3});
tl3.add(tweenTl3)
const scene3 = new ScrollMagic.Scene(
    {
        triggerElement: "#portfolio",
    }
).setTween(tl3)
.addTo(controller)